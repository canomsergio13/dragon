import Card from "../components/Card"
import items from '../src/cards.json'

const Home = () => {

    return (
        <section className="d-flex justify-content-around align-items-center pb-5 flex-wrap" >
            {
                items.map(item => 
                    <Card 
                        key={item.id} 
                        img={item.img}
                        title={item.title}
                        description={item.description}
                        dashboard={item.dashboard}
                        web={item.web}
                    />
                )
            }
        </section>
    )
}

export default Home