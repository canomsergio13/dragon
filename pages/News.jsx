import { useEffect, useState } from "react";
import { getNews } from "../api/news";
import CardNew from "../components/news/CardNew";
import { Heading } from "@chakra-ui/react";

  
  
const News = () => {
      
    const [News, setNews] = useState(null)

    useEffect(() => {
        getData()
    }, [])

    const getData = async () => {
        const res = await getNews();
        setNews(res?.data)
    }
    
    return (
        <div className="my-5" >
            <Heading className="text-center mb-4">Notícias</Heading>
            {
                News?.map( n =>
                    <CardNew
                        id={n.id}
                        key={n.id}
                        date={n.date}
                        allNews={true}
                        title={n.title}
                        author={n.author}
                        description={n.description}
                    />
                )
            }
        </div>
    )
}



export default News