import ReactApexChart from 'react-apexcharts';
import { Card, CardBody } from '@chakra-ui/react'
import { useEffect,useState } from 'react'
import Search from '../components/trak/Search';
import CardStatics from '../components/trak/CardStatics';
import { ColorBlue, ColorGreen, ColorOrange, ColorRed } from '../helpers/getColors';
import { GeneralData } from '../helpers/getOptionsApex';
import { getLogsReport } from '../api/logs';

const DashboardTrak = () => {

    const [filterAux, setFilterAux] = useState({
        filter: '',
        initialDate: '',
        finalDate: ''
    })

    const [DataCard, setDataCard] = useState(null)

    useEffect(()=>{getData()},[filterAux.initialDate,filterAux.finalDate])

    const changeAUX = ({target}) => {
        const { name, value} = target;
        setFilterAux({
            ...filterAux,
            [name] : value
        })
    }

    const handleDate = value => {
        setFilterAux({
            ...filterAux,
            filter: value
        })
    }

    const [serie, setSeries] = useState({series:[],options:{}})
    const [seri, setSeri] = useState({series:[],options:{}})

    const getData = async () => {

        if(filterAux.initialDate !== '' || filterAux.finalDate !== '' || filterAux.filter === ''){
            if(filterAux.filter === 'Rango' && filterAux.finalDateAUX === '') return;
            const res = await getLogsReport(filterAux,{},'Report');
            setDataCard(res.data)
            const resGen = await getLogsReport(filterAux,{},'General');

            const series1 = [
                {
                  name:'PORCENTAJE PRODUCTIVIDAD',
                  data:resGen?.data[0]?.productivoPercentages || []
                },{
                  name:'PORCENTAJE NO PRODUCTIVIDAD',
                  data:resGen?.data[0]?.noProductivoPercentages || []
                }
            ]

            const series2 = [
                {
                  name:'TIEMPO PRODUCTIVO',
                  data:resGen?.data[0]?.productiveTimes || []
                },{
                  name:'TIEMPO NO PRODUCTIVO',
                  data:resGen?.data[0]?.noProductiveTimes || []
                }
            ]

            const categories = resGen?.data[0]?.dates || [];
            const colors1 = [ColorBlue,ColorOrange]
            const colors2 = [ColorGreen,ColorRed]
            let Horiz = false;
            if(resGen?.data[0]?.dates?.length >10)Horiz = true;

            const DataResGen = GeneralData(series1,categories,colors1,Horiz,'%');
            const DataResGe =  GeneralData(series2,categories,colors2,Horiz,'hrs');

            setSeries(DataResGen)
            setSeri(DataResGe)
        }
    }

    const dataProps = {
        handleDate,
        changeAUX,
        filterAux,
    }

    return (
        <div className='vh-100 px-2 mt-4'>
            <section >
                {/* Busqueda */}
                <Search {...dataProps} />

                {/* Cards */}
                <section className='mt-3 d-flex justify-content-center text-center'>
                    { DataCard && DataCard.map(card=> <CardStatics key={card.id} cards={card} /> ) }
                </section>

                {/* Gráfica */}
                <section  className='mt-3' >
                    <Card style={{ backgroundColor:'F0F2F5 '}} >
                        <CardBody>
                            <div id="chart">
                                <ReactApexChart 
                                    options={serie?.options} 
                                    series={serie?.series} 
                                    type="bar" 
                                    height={500} />
                            </div>
                        </CardBody>
                    </Card>

                    <Card className='mt-3' style={{ backgroundColor:'F0F2F5 '}}>
                        <CardBody>
                            <div id="chart2">
                                <ReactApexChart 
                                    options={seri?.options} 
                                    series={seri?.series} 
                                    type="bar" 
                                    height={500} />
                            </div>
                        </CardBody>
                    </Card>
                </section>

            </section>

        </div>
    )
}

export default DashboardTrak