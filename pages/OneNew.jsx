import { useEffect, useState } from "react";
import { useParams } from "react-router-dom"
import { getNewById } from "../api/news";
import CardNew from "../components/news/CardNew";

const OneNew = () => {
    const { id } = useParams();

    const [NewID, setNewID] = useState(null)

    useEffect(() => {
        getData()
    }, [])

    const getData = async () => {
        const res = await getNewById(id);
        setNewID(res.data)
    }

    return (
        <div>
            {
                NewID && 
                    <CardNew
                        id={NewID.id}
                        key={NewID.id}
                        allNews={false}
                        date={NewID.date}
                        title={NewID.title}
                        author={NewID.author}
                        content={NewID.content}
                        description={NewID.description}
                    />
            }

        </div>
    )
}

export default OneNew