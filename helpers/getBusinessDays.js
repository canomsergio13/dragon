import moment from 'moment'

export const getBusinessDays = (startDate, endDate) => {
    
    let currentDate = moment(startDate);
    const end = moment(endDate);
    let businessDays = 0;

    while (currentDate <= end) {
        const dayOfWeek = currentDate.isoWeekday();
        if (dayOfWeek <= 5) businessDays++;
        currentDate = currentDate.add(1, 'days');
    }

    return businessDays;
} 

