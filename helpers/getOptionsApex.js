import { convertSeconds } from "./getTimeFormatted";

export const GeneralData = (series,categories,colors,Horiz,hrs) => {
    return {
        series,
        options:{
            chart: {
                type: 'bar',
                height: 500,
                stacked: true,
                toolbar: { show: true },
                zoom: { enabled: true }
            },
            responsive: [
                {
                    breakpoint: 480,
                    options: {
                        legend: {
                            position: 'bottom',
                            offsetX: -10,
                            offsetY: 0
                        }
                    }
                }
            ],
            plotOptions: {
                bar: {
                    horizontal: Horiz,              
                    dataLabels: {
                        total: {
                            enabled: true,
                            style: {
                                fontSize: '15px',
                                fontWeight: 900
                            }
                        }
                    }
                }
            },
            xaxis: { categories },
            legend: {
                position: 'top',
                offsetY: 0,
                fontSize: '15px',
                fontWeight: 700
                
            },
            fill: { opacity: 1 },
            colors,
            tooltip: {
                shared: false,
                y: {
                  formatter: function (value) {
                        if(hrs === 'hrs') return convertSeconds(value);
                        return value + ' %';
                  }
                }
            }
        }
    }
}