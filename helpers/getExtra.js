export const getExtraData = id => {
    const extra = {
        '1': {
            textBadge: '#',
            colorBadge: 'blue',
            colorText: 'blue.600'
        },
        '2': {
            textBadge: '#',
            colorBadge: 'green',
            colorText: 'green.500'
        },
        '3': {
            textBadge: '#',
            colorBadge: 'red',
            colorText: 'red.600'
        },
        '4': {
            textBadge: '%',
            colorBadge: 'purple',
            colorText: 'purple.700'
        },
        '5': {
            textBadge: '%',
            colorBadge: 'volcano',
            colorText: 'red.600'
        },
    }

    return extra[id]
}

export const getTypeFirstInput = (type="date") => {
    const types = {
        "Dia": "date",
        "Semana": "week",
        "Mes": "Month",
        "Rango": "date" 
    }

    return types[type];
}
