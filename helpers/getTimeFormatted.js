export const convertSeconds = seconds => {
    let hours = Math.floor(seconds / 3600);
    seconds = seconds % 3600;
    let minutes = Math.floor(seconds / 60);
    seconds = seconds % 60;
    let result = '';

    (hours) && (result += hours + 'h ');
    (minutes) && (result += minutes + 'm ');
    
    result += seconds + 's';
    return result;
}