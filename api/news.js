import axios from "axios"
import { __SERVER_API__ } from "../helpers/getVars"

export const getNews = async () => {
    try {
        const res = axios.get(`${__SERVER_API__}/news`);
        return res;    
    } catch (error) {
        console.log(error)
    }
}

export const getNewById = async id => {
    try {
        const res = axios.get(`${__SERVER_API__}/news/${id}`);
        return res;    
    } catch (error) {
        console.log(error)
    }
}