import moment from "moment";
import { getBusinessDays } from "../helpers/getBusinessDays";
import axios from "axios";
import { __SERVER_TRAK__ } from "../helpers/getVars";

export const getLogsReport = async (filterAux,users,route) => {
    const {initialDate,finalDate,filter} = filterAux;

    const package_id = 1; 

    let startOf = '';
    let endOf   = '';
    let query = '';

    if(filter === 'Semana' || filter === 'Mes'){
        if(filter === 'Semana'){
            const week = moment(initialDate)
            startOf = week.startOf('week').add(1,'days').format('YYYY-MM-DD')
            endOf = week.endOf('week').add(1,'days').format('YYYY-MM-DD');
        }
        if(filter === 'Mes'){
            const month = moment(`${initialDate}-01`);
            startOf = `${initialDate}-01`;
            endOf = month.endOf('month').format('YYYY-MM-DD');
        }
        query = `startOf=${startOf}&endOf=${endOf}&avg=${getBusinessDays(startOf,endOf)}`;
    }

    if(filter === ''){
        startOf = moment().format('YYYY-MM-DD');
        endOf = moment(startOf).subtract(10,'days').format('YYYY-MM-DD');
        query = `startOf=${endOf}&endOf=${startOf}&avg=${getBusinessDays(endOf,startOf)}`; 
    }

    if(filter === 'Dia'){
        query = `startOf=${initialDate}&avg=${1}`;
    }

    if(filter === 'Rango'){
        query = `startOf=${initialDate}&endOf=${finalDate}&avg=${getBusinessDays(initialDate,finalDate)}`;
    } 

    if(route==='Report' ) route='getLogsReport';
    if(route==='General') route='getLogsGeneral';
    
    try {
        const res = await axios.post(`${__SERVER_TRAK__}/package/${package_id}/${route}?${query}`,{users});
        return res
    } catch (error) {
        console.log(error)
    }
}