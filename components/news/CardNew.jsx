import { useNavigate } from 'react-router-dom';
import {FcManager} from 'react-icons/fc';
import PropTypes from 'prop-types';
import {
    Box,
    Heading,
    Link,
    Image,
    Text,
    useColorModeValue,
    Container,
    HStack,
    VStack
} from '@chakra-ui/react';

const BlogAuthor= props => {
    return (
      <HStack marginTop="2" spacing="2" display="flex" alignItems="center">
        <FcManager size={32} />
        <Text fontWeight="medium" className="mb-0">{props.name}</Text>
        <Text className="mb-0">—</Text>
        <Text className="mb-0">{props.date}</Text>
      </HStack>
    );
};

function convertirTexto(texto) {
    // Remover caracteres especiales y convertir todo a minúsculas
    const textoLimpio = texto.toLowerCase().replace(/[^a-zA-Z0-9 ]/g, "");
  
    // Dividir el texto en palabras
    const palabras = textoLimpio.split(" ");
  
    // Tomar las primeras 10 palabras
    const primerasPalabras = palabras.slice(0, 10);
  
    // Unir las palabras con guiones
    const textoConvertido = primerasPalabras.join("-");
  
    return textoConvertido;
}

const CardNew = props => {

    const navigate = useNavigate()

    return (
        <Container maxW={'7xl'} px="5" style={{ cursor: props.allNews ? 'pointer' : '' }} onClick={()=> {
        
            if(props.allNews){
                navigate(`/new/${convertirTexto(props.title)}/${props.id}`)
            }
        
        }}>
            <Box
                marginTop={{ base: '1', sm: '5' }}
                display="flex"
                flexDirection={{ base: 'column', sm: 'row' }}
                justifyContent="space-between"
            >
            <Box
                display="flex"
                flex="1"
                marginRight="3"
                position="relative"
                alignItems="center"
            >
                <Box
                    width={{ base: '100%', sm: '85%' }}
                    zIndex="2"
                    marginLeft={{ base: '0', sm: '5%' }}
                    marginTop="5%"
                >
                    <Link textDecoration="none" _hover={{ textDecoration: 'none' }}>
                        <Image
                            borderRadius="lg"
                            src={
                                'https://images.unsplash.com/photo-1499951360447-b19be8fe80f5?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=800&q=80'
                            }
                            alt="some good alt text"
                            objectFit="contain"
                        />
                    </Link>
                </Box>

                <Box zIndex="1" width="100%" position="absolute" height="100%">
                    <Box
                        bgGradient={useColorModeValue(
                            'radial(orange.600 1px, transparent 1px)',
                            'radial(orange.300 1px, transparent 1px)'
                        )}
                        backgroundSize="20px 20px"
                        opacity="0.4"
                        height="100%"
                    />
                </Box>

            </Box>
            <Box
                display="flex"
                flex="1"
                flexDirection="column"
                justifyContent="center"
                marginTop={{ base: '3', sm: '0' }}
            >
                <Heading marginTop="1">
                    <Link textDecoration="none" _hover={{ textDecoration: 'none' }}>
                        {props.title}
                    </Link>
                </Heading>
                <Text
                    as="p"
                    marginTop="2"
                    color={useColorModeValue('gray.700', 'gray.200')}
                    fontSize="lg"
                >
                    {props.description}
                </Text>
                <BlogAuthor
                    name={props.author}
                    date={props.date}
                />
                </Box>
            </Box>

            {
                props?.content && (
                    <VStack paddingTop="40px" spacing="2" alignItems="flex-start" marginBottom={5} >
                        <Text as="p" fontSize="lg">
                            {props.content}
                        </Text>
                    </VStack>
                )
            }
        </Container>
    )
}

BlogAuthor.propTypes = {
    name: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired
}

CardNew.propTypes = {
    description: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    allNews: PropTypes.bool.isRequired,
    date: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    content: PropTypes.string,
}

export default CardNew