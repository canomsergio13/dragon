import { Card, CardBody, Text } from '@chakra-ui/react'
import { getExtraData } from '../../helpers/getExtra';
import PropTypes from 'prop-types'
import { Badge } from 'antd'

const CardStatics = ({ cards }) => {
    const extras = getExtraData(cards.id);
    return (
        <section style={{width:'20%',paddingRight:15}}>
            <Badge.Ribbon text={extras.textBadge} color={extras.colorBadge}>
                <Card style={{width:'100%' , backgroundColor: '#F0F2F5'}}>
                    <CardBody>
                        <Text fontSize='16px' as='b' lineHeight={1}>
                            {cards.text}
                        </Text>
                        <div className='mt-3'> 
                            <Text fontSize='22px' as='b' color={extras.colorText} lineHeight={1}>
                                {cards.value}
                            </Text>
                        </div>
                    </CardBody>
                </Card>
            </Badge.Ribbon>
        </section>
    )
}

CardStatics.propTypes = {
    cards: PropTypes.object.isRequired
}

export default CardStatics