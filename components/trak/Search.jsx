import { getTypeFirstInput } from '../../helpers/getExtra';
import { Card, CardBody } from '@chakra-ui/react'
import PropTypes from 'prop-types';
import { Select } from 'antd'

const Search = (props) => {
    const { 
        handleDate,
        changeAUX,
        filterAux
    }  = props;

    return (
        <section>
            <Card style={{ backgroundColor: '#F0F2F5' }} >
                <CardBody>
                    <div className='d-flex justify-content-center'>
                        <div className='d-flex flex-column align-items-center w-25 px-4'>
                            <label>Filtro por:</label>
                            <Select
                                defaultValue=""
                                style={{ width: '100%'}}
                                allowClear
                                onChange={handleDate}
                                options={[
                                    {
                                        value: 'Dia',
                                        label: 'Día',
                                    },{
                                        value: 'Semana',
                                        label: 'Semana',
                                    },{
                                        value: 'Mes',
                                        label: 'Mes',
                                    },{
                                        value: 'Rango',
                                        label: 'Rango',
                                    },
                                ]}
                            />
                        </div>

                        <div className='d-flex flex-column align-items-center w-25 px-4'>    
                            <label>Fecha Inicial</label>
                            <input 
                                style={{width:'75%'}}
                                type={getTypeFirstInput(filterAux.filter)} 
                                name="initialDate" 
                                onChange={changeAUX} 
                                disabled={(filterAux.filter === "")?true:false } />
                        </div>
                        {
                            (filterAux.filter === 'Rango') && <div className='d-flex flex-column align-items-center w-25'>    
                                <label>Fecha Final</label>
                                <input type='date' name="finalDate" onChange={changeAUX} />
                            </div>
                        }
                    </div>
                </CardBody>
            </Card>
        </section>
    )
}

Search.propTypes = {
    handleDate: PropTypes.func.isRequired,
    changeAUX: PropTypes.func.isRequired,
    filterAux: PropTypes.object.isRequired
}

export default Search