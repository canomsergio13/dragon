import { Link, Outlet } from 'react-router-dom';

const Navbar = () => 
    <section>
        <nav className="navbar navbar-expand-lg navbar-dark bg-success px-5">
            <div className="container-fluid">
                <Link to="/" className="navbar-brand d-flex align-items-center">
                    <img src='/dragon-favicon.png' className='mx-1' />
                    Dragón
                </Link>
                <button type="button" className="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarCollapse4">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarCollapse4">
                    <section className="navbar-nav">
                        <a 
                            target='_blank' 
                            rel='noreferrer' 
                            href="https://glpi.victum-re.online/" 
                            className="navbar-brand"
                        >Mesa de ayuda</a>
                        <a 
                            target='_blank' 
                            rel='noreferrer' 
                            href="https://trak.victum-re.online/" 
                            className="navbar-brand"
                        >VictumTrak</a>
                        <a 
                            target='_blank' 
                            rel='noreferrer' 
                            href="https://us5-cloud.acronis.com/login" 
                            className="navbar-brand"
                        >Ciberseguridad</a>
                    </section>
                    <section className="d-flex ms-auto navbar-nav">
                        <Link to='/news' className="navbar-brand">Blog</Link>
                        <Link to='/news' className="navbar-brand">Noticías/Avisos</Link>
                    </section>
                </div>
            </div>        
        </nav>

        <main className='container'>
            <Outlet />
        </main> 
    </section>

export default Navbar