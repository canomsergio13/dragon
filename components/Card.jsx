import { FcAreaChart, FcMultipleDevices } from 'react-icons/fc'
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import {
  Box,
  Center,
  Heading,
  Text,
  Stack,
  useColorModeValue,
  Button,
} from '@chakra-ui/react';

export default function Card(props) {
    const navigate = useNavigate()
    const {
        img, web,
        dashboard,
        title,
        description
    } = props;

    const handleRedirect = to => window.open(to, '_blank')

    const handleNavigate = to => navigate(to)

    return (
        <Center mt={12}>
            <Box
                maxW={'350px'}
                minW={'250px'}
                w={'full'}
                bg={useColorModeValue('white', 'gray.900')}
                boxShadow={'2xl'}
                rounded={'md'}
                p={6}
                className='card__img'
            >
                <Box
                    h={'210px'}
                    bg={'gray.100'}
                    mt={-6}
                    mx={-6}
                    pos={'relative'}
                    overflow={'hidden'}
                >
                    <img
                        src={img}
                        style={{ height: '100%' }}
                        alt={title}
                    />
                </Box>

                <Stack mt={5}>
                    <Heading
                        color={useColorModeValue('gray.700', 'white')}
                        fontSize={'2xl'}
                        fontFamily={'body'}>
                        {title}
                    </Heading>
                    <Text color={'gray.800'}>
                        {description}
                    </Text>
                </Stack>

                <Stack mt={2} direction={'row'} spacing={4}>
                    <Button
                        flex={1}
                        fontSize='md'
                        rounded='full'
                        bg='purple.100'
                        _focus={{ bg: 'purple.200'}}
                        _hover={{ bg: 'purple.200'}}
                        onClick={()=>handleRedirect(web)}
                    >
                        <FcMultipleDevices className='mx-1' />
                        Ir al Portal
                    </Button>
                    {
                        dashboard?.view
                            && (
                            
                            <Button
                                flex={1}
                                fontSize='md'
                                rounded='full'
                                bg='green.400'
                                color='white'
                                boxShadow='0px 1px 25px -5px rgb(66 153 225 / 48%), 0 10px 10px -5px rgb(66 153 225 / 43%)'
                                _hover={{ bg: 'green.500'}}
                                _focus={{ bg: 'green.500'}}
                                onClick={()=>handleNavigate(dashboard.to)}
                            >
                                <FcAreaChart  className='mx-1' />
                                Dashboard
                            </Button>)
                    }
                </Stack>
            </Box>
        </Center>
    );
}


Card.propTypes = {
    img: PropTypes.string.isRequired,
    web: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    dashboard: PropTypes.object.isRequired
}