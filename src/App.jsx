import { BrowserRouter, Route, Routes } from 'react-router-dom'
import DashboardTrak from '../pages/DashboardTrak'
import 'bootstrap/dist/css/bootstrap.min.css'
import Home from '../pages/home'
import Navbar from '../components/Navbar'
import News from '../pages/News'
import OneNew from '../pages/OneNew'

function App() {

    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Navbar />}>
                    <Route path="" element={<Home />} />
                    <Route path="/dashboard-trak" element={<DashboardTrak />} />
                    <Route path="/news" element={<News />} />
                    <Route path="/new/:title/:id" element={<OneNew />} />
                </Route>
            </Routes>
        </BrowserRouter>
    )
}

export default App
